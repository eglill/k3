import java.util.LinkedList;
import java.util.NoSuchElementException;

public class DoubleStack {

   private LinkedList<Double> doubleStack;

   public static void main (String[] argum) {
   }

   DoubleStack() {
      doubleStack = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack temporary = new DoubleStack();

      for (int i = doubleStack.size() - 1; i >= 0; i--)
         temporary.push(doubleStack.get(i));
      return temporary;
   }

   public boolean stEmpty() {
      return doubleStack.isEmpty();
   }

   public void push (double a) {
      // Adding element to stack on first place.
      doubleStack.addFirst(a);
   }

   public double pop() {
      // Returning and removing first element.
      if (doubleStack.size() < 1) {
         throw new NoSuchElementException("Stack is empty!");
      }
      return doubleStack.removeFirst();
   }

   public void op (String s) {
      //  Arithmetics between first two elements.
      if (this.size() < 2) {
         if (this.size() == 1) {
            throw new IndexOutOfBoundsException("Cannot perform \"" + s + "\" with one element");
         }
         throw new IndexOutOfBoundsException("Cannot perform arithmetic operation on empty stack");
      }
      double second = this.pop();
      double first = this.pop();
      double value;
      switch (s.toLowerCase()) {
         default:
            throw new IllegalStateException("illegal symbol \"" + s + "\" ");
         case "+":
            value = first + second;
            break;
         case "-":
            value = first - second;
            break;
         case "*":
            value = first * second;
            break;
         case "/":
            value = first / second;
            break;
      }
      push(value);
   }
  
   public double tos() {
      // Reads first without removing.
      if (this.size() < 1)
         throw new NullPointerException ("Stack is empty.");
      return doubleStack.getFirst();
   }

   @Override
   public boolean equals (Object o) {
      if (((DoubleStack) o).size() == this.size()) {
         for (int i = 0; i < this.size(); i++) {
            if (((DoubleStack) o).get(i) != this.get(i))
               return false;
         }
         return true;
      }
      return false;
   }

   @Override
   public String toString() {
      StringBuilder string = new StringBuilder();
      for (int i = this.size() - 1; i >= 0; i--) {
         if (i == 0) {
            string.append(doubleStack.get(i));
         } else {
            string.append(doubleStack.get(i)).append(", ");
         }
      }
      return string.toString();
   }

   public static double interpret (String pol) {
      DoubleStack stack = new DoubleStack();

      if (pol.isEmpty()) throw new RuntimeException("Given string is empty.");

      String stripped = pol.replaceAll("\\s+", " ").trim();
      String[] elements = stripped.split(" ");

      for (String element : elements) {
         try {
            // If element is numeric, we push it into stack.
            stack.push(Double.parseDouble(element));
         } catch (NumberFormatException e) {
            // If element is not numeric, it must be operator.
            if (stack.swapRotDup(element)) {
               continue;
            }
            try {
               stack.op(element);
            } catch (IndexOutOfBoundsException e1) {
               throw new IndexOutOfBoundsException(e1.getMessage() + " in expression \"" + pol +"\"");
            } catch (IllegalStateException e2) {
               throw new IllegalStateException(e2.getMessage() +  " in expression \"" + pol +"\"");
            }
         }
      }
      if (stack.size() > 1) {
         throw new RuntimeException("Numbers and operations are not balanced: " + pol);
      }
      return stack.tos();
   }

   private int size() {
      // Return size of stack.
      return doubleStack.size();
   }

   private double get(int i) {
      // Returns stack element at a given index.
      if (i < 0 || this.size() - 1 < i)
         throw new IndexOutOfBoundsException("Index out of bounds.");
      return doubleStack.get(i);
   }

   private boolean swapRotDup(String element) {
      double second;
      double first;
      double third;
      switch (element.toLowerCase()) {
         case "swap":
            if (doubleStack.size() < 2) {
               throw new IllegalStateException("Can not perform SWAP with les than two elements in stack: "  + doubleStack);
            } else {
               first = doubleStack.pop();
               second = doubleStack.pop();
               doubleStack.push(first);
               doubleStack.push(second);
            }
            return true;
         case "rot":
            if (doubleStack.size() < 3) {
               throw new IllegalStateException("Can not perform ROT with les than three elements in stack: "  + doubleStack);
            } else {
               first = doubleStack.pop();
               second = doubleStack.pop();
               third = doubleStack.pop();
               doubleStack.push(second);
               doubleStack.push(first);
               doubleStack.push(third);
            }
            return true;
         case "dup":
            if (doubleStack.size() < 1) {
               throw new IllegalStateException("Can not perform ROT with les than one element in stack: "  + doubleStack);
            } else {
               first = doubleStack.pop();
               doubleStack.push(first);
               doubleStack.push(first);
            }
            return true;
      }
      return false;
   }

}

